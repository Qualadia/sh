{
  inputs.nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
  outputs = { self, nixpkgs }:
    with nixpkgs.legacyPackages.x86_64-linux; {
      flecs = stdenv.mkDerivation {
        pname = "flecs";
        version = "3.1.3";
        nativeBuildInputs = [ cmake ];
        src = fetchFromGitHub {
          owner = "SanderMertens";
          repo = "flecs";
          rev = "v3.1.3";
          sha256 = "sha256-NgyRHq9XkuO8l2VxUGyDXGMEm9BtilnM//OesdQtfOU=";
        };
      };
      
      raylib-cpp = stdenv.mkDerivation {
        pname = "raylib-cpp";
        version = "4.2.8";
        nativeBuildInputs = [ cmake ];
        src = fetchFromGitHub {
          owner = "RobLoach";
          repo = "raylib-cpp";
          rev = "v4.2.8";
          sha256 = "sha256-p93VdnU7R6WASNs4vGvJguFYaC2QqrJtpq9vFjsMn6U=";
        };
        buildInputs = [ raylib ];
      };

      box2d = stdenv.mkDerivation {
        pname = "box2d";
        name = "box2d";
        version = "2.4.1";
        nativeBuildInputs = [ cmake pkg-config ];

        cmakeFlags = [
          "-DBOX2D_INSTALL=ON"
          "-DBOX2D_BUILD_SHARED=ON"
          "-DBOX2D_BUILD_EXAMPLES=OFF"
          "-DBOX2D_BUILD_TESTBED=OFF"
          "-DBOX2D_BUILD_UNIT_TESTS=OFF"
        ];      
        src = fetchFromGitHub{
          owner = "erincatto";
          repo = "box2d";
          rev = "v2.4.1";
          sha256 = "sha256-cL8L+WSTcswj+Bwy8kSOwuEqLyWEM6xa/j/94aBiSck=";
        };
      };

      raylibW = raylib.overrideAttrs (old: {
          cmakeFlags =
            raylib.cmakeFlags
            ++ [
              "-DUSE_WAYLAND=ON"
            ];
          buildInputs = [ 
           mesa glfw-wayland xorg.libXi xorg.libXcursor xorg.libXrandr xorg.libXinerama
            alsa-lib libpulseaudio];
        });
      packages.x86_64-linux.default = stdenv.mkDerivation {
        name = "SH";
        src = ./.;
        nativeBuildInputs = [ cmake ];
        NIX_CFLAGS_COMPILE = [ "-I${imgui}/include/imgui" ];
        installPhase = "mkdir $out/bin; cp -r ./* $out/bin/";
        buildInputs = [
          lua
          self.raylibW
          #box2d
          #imgui
          self.box2d
          glfw
          glfw-wayland
          self.flecs
          self.raylib-cpp
          #self.rlImGui
        ];
      };
    };
}
