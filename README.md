# Project overview

## Naming Conventions
| What                          | First character | Case       | Additional conventions |
|-------------------------------|-----------------|------------|------------------------|
| Class                         | Uppercase       | Camelcase  |                        |
| Struct                        | Uppercase       | Camelcase  |                        |
| Subfolder                     | Lowercase       | Camelcase  |                        |
| Files(both .hpp and .cpp)     | Lowercase       | Snake\_case|                        |
| Classe's and struct's field   | Lowercase       | Camelcase  |                        |
| Global variable               | Lowercase       | Camelcase  | Starts with g_         |
| Global function               | Uppercase       | Camelcase  |                        |
| Function in class or struct   | Uppercase       | Camelcase  |                        |
| Global and important constant | Uppercase       | Caps       |                        |
| Local variable                | Lowercase       | Camelcase  |                        |

## Project structure

```
root
├── CMakeLists.txt
├── README.md
├── build
├── compile_flags.txt
├── include
│   ├── flecs.o
│   └── glfw3.h
├── resources
│   ├── assets
│   │   ├── anim.png
│   │   ├── animation.png
│   │   ├── eyecube2.png
│   │   └── player.png
│   └── shaders
│       ├── light_shader.fsh
│       └── light_shader.vsh
└── src
    ├── content
    ├── engine
    │   ├── components
    │   │   └── DrawableComponent.hpp
    │   ├── core
    │   │   ├── FramePipeline.cpp
    │   │   └── FramePipeline.hpp
    │   └── util
    │       ├── AdvancedTextureAtlas.hpp
    │       ├── AtlasZone.hpp
    │       └── TextureAtlas.hpp
    └── main.cpp
```

## TODO
- [X] Basic frame pipeline
- [X] Core components
  - [X] Drawable components
  - [X] Position
  - [X] Input
  - [X] Hitbox
  - [X] Logic
- [X] Core systems
  - [X] Rendering
  - [X] Computing game logic
  - [X] Input
  - [X] Collision
- [ ] Save system
  - [ ] Saving settings
  - [ ] Saving game save
  - [ ] Loading settings
  - [ ] Loading game save
  - [ ] Loading game data(levels)
- [ ] Gui system
- [ ] Entity creating system
  - [ ] Player
    - [ ] Player starup
    - [ ] Player update loop
  - [ ] Bosses
    - [ ] AI loading(from lua file?)
- [ ] Additional components
  - [ ] 3D objects
    - [ ] Meshes
    - [ ] Textures
  - [ ] Light
  - [ ] Point hitbox component
- [ ] Additional systems
  - [X] Debug system for rendering additional info
  - [ ] Rendering 3D objects
  - [ ] Rendering Light
  - [ ] Point collision detection



## BACKYARD TODO
- Rework collision system to have more flexibility
