#version 330
in vec3 vertexPosition;
in vec2 vertexTexCoord;
in vec2 vertexTexCoord2;
in vec4 vertexColor;

uniform mat4 mvp;

out vec4 Color;
out vec2 TexCoord;

void main() {
    
    

  Color = vertexColor;
  TexCoord = vertexTexCoord;
  
  gl_Position = mvp * vec4(vertexPosition, 1.0);


}
