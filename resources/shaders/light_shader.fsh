#version 330

in vec4 Color;
in vec2 TexCoord;



uniform sampler2D texture0;
uniform vec4 colDiffuse;



uniform vec2 LightPosition;
uniform float LightRadius;

vec4 LightColor = vec4(1.0, 0.94, 0.29, 0.68);

out vec4 FragColor;


void main() {
  
  //FragColor = texture(texture0, TexCoord) * Color;

  float dist = distance(TexCoord, LightPosition);
  float part = dist/LightRadius;
  // float customEffect = 0.3 * abs(cos(2.0 * u_time));
  //float sum = part + customEffect;
  float clamp = clamp(1.0 - part, 0.0, 1.0);
  FragColor = mix(texture(texture0, TexCoord), LightColor, clamp) * Color;

}
