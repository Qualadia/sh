#include <raylib-cpp.hpp>
#include <raymath.hpp>
#include <flecs.h>
#include <string>
#include <iostream>
#include <vector>
#include "engine/core/frame_pipeline.hpp"
#include "engine/components/position_component.hpp"
#include "engine/components/drawable_component.hpp"
#include "engine/components/hitbox_component.hpp"
#include "engine/components/input_component.hpp"
#include "engine/components/logic_component.hpp"
#include "engine/components/camera_component.hpp"
#include "engine/core/engine_controller.hpp"
#include "imgui.h"
#include "rlImGui.h"

int main() {
  EngineController* engine = new EngineController();
  std::vector<AtlasZone> z = {
    {
    0,
    0,
    128,
    384,
    64,
    64,
    {2,4,6,8,10}}
  };
  AdvancedTextureAtlas* test = new AdvancedTextureAtlas{
    "../resources/assets/eyecube2.png",
    {z}
  };
  raylib::Camera2D cam = {};
  cam.target = {-static_cast<float>(GetScreenWidth()/2 - 50.0), -static_cast<float>(GetScreenHeight()/2 - 50.0)};
  cam.offset = {0.0, 0.0};
  cam.rotation = 0.0f;
  cam.zoom = 1.0f;
  b2PolygonShape box;
  box.SetAsBox(32.0f, 32.0f);
  flecs::world* ecs = engine->GetWorld();
  auto e2 = ecs->entity()
    .set<Position>({{150, 100}, {0,0}, 1})
    .set<Hitbox>({1, &box, b2_dynamicBody})
    .set<DrawableSprite>({test, 0, 3});
  auto e3 = ecs->entity()
    .set<Position>({{125,25}, {0,0}, 1})
    .set<Hitbox>({1, &box, b2_dynamicBody})
    .set<CameraDynamic>(cam)
    .set<Input>({
      {KEY_A, KEY_W, KEY_S, KEY_D},{KEY_A, KEY_W, KEY_S, KEY_D},{},{},{},{},{},{},{}
      })
    .set<DrawableAnimation>({test, 0, 1, 8, 10.0f, {1.0f, 1.0f}, 0, 0});
  auto e4 = ecs->entity()
    .set<Position>({{300, 320}, {0,0}, 1})
    .set<Hitbox>({1, &box, b2_staticBody})
    .set<DrawableSprite>({test, 0, 1});
  b2PolygonShape walls;
  b2Vec2 pts[4] = {{-450.0f,1000.0f}, {-450.0f, 500.0f}, {2000.0f, 500.0f}, {2000.0f, 1000.0f}};
  walls.Set(pts, 4);
  auto wall = ecs->entity()
    .set<Position>({{0,0}, {0,0}, 1})
    .set<Hitbox>({1, &walls});
   //Shader testsh = LoadShader("resources/shaders/light_shader.vsh", "resources/shaders/light_shader.fsh");
   //int lightpos = GetShaderLocation(testsh, "LightPosition");
   //int lightradius = GetShaderLocation(testsh, "LightRadius");
  e3.set(Logic([e3] () mutable {
    const Input *inp = e3.get<Input>();
    const Camera2D cam = e3.get<CameraDynamic>()->camera;
    const Position *p = e3.get<Position>();
    float mov_power = 155000 * GetFrameTime();
    // float delta = static_cast<float>(inp->keyboardPressedKeys.at(KEY_A)) * 50 * GetFrameTime();
    raylib::Vector2 delta = {0.0, 0.0};
    // float delta = 0.0;
    // delta += static_cast<float>(inp->keyboardPressedKeys.at(KEY_D)) * mov_power;
    if (inp->keyboardPressedKeys.at(KEY_D)) delta.x += mov_power;
    if (inp->keyboardPressedKeys.at(KEY_A)) delta.x -= mov_power;
    if (inp->keyboardPressedKeys.at(KEY_S)) delta.y += mov_power;
    if (inp->keyboardPressedKeys.at(KEY_W)) delta.y -= mov_power;
    Camera2D newcam = Camera2D({});
    newcam.offset = cam.offset;
    newcam.rotation = cam.rotation;
    newcam.target = {-static_cast<float>(GetScreenWidth()/2 - p->position.x), -static_cast<float>(GetScreenHeight()/2 - p->position.y)};
    newcam.zoom = cam.zoom;
    e3.set<CameraDynamic>({newcam});
    // std::cout << delta.x << std::endl;
    e3.get<Position>()->body->ApplyLinearImpulseToCenter({delta.x, delta.y}, true);
  }));

  engine->SetPlayer(&e3);
  engine->StartGameLoop();
  return 0;
}
