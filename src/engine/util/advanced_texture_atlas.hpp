#pragma once

#include <raylib-cpp.hpp>
#include <string>
#include <algorithm>
#include "atlas_zone.hpp"

struct AdvancedTextureAtlas {
  const Texture2D atlas;
  AdvancedTextureAtlas(std::string fileName, std::vector<AtlasZone> &zones) : atlas{LoadTexture(fileName.c_str())} {
    for( unsigned int i = 0; i < zones.size(); i++ ) {
      std::vector<Rectangle> temp = {};
      int currentID = 0;
      for( unsigned int j = 0; j < zones[i].height; j += zones[i].spriteHeight ) {
        for( unsigned int o = 0; o < zones[i].width; o += zones[i].spriteWidth ) {
          if(!std::binary_search(zones[i].except.begin(), zones[i].except.end(), currentID)) {
            temp.push_back({
              (float)zones[i].x + o,
              (float)zones[i].y + j,
              (float)zones[i].spriteWidth,
              (float)zones[i].spriteHeight});
          };
          currentID++;
        };
      };
      sheet.push_back(temp);
    };
  };

  ~AdvancedTextureAtlas() {
    UnloadTexture(atlas);
  };

  Rectangle GetTextureRect(unsigned int zone, unsigned int id) {
    return sheet[zone][id];
  };
  private:
    std::vector<std::vector<Rectangle>> sheet;

};




