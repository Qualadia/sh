#pragma once
#include <vector>

struct AtlasZone {
  unsigned int x, y, width, height, spriteWidth, spriteHeight;
  std::vector<unsigned int> except;
  int GetSpritesAmount() {
    int count = 0;
    count += width/spriteWidth;
    count += height/spriteHeight;
    count -= except.size();
    return count;
  };
};
