#pragma once
#include <flecs.h>
#include "../components/position_component.hpp"
#include "../components/hitbox_component.hpp"


int compareHitboxLayer(ecs_entity_t e1, const Hitbox *h1, ecs_entity_t e2, const Hitbox *h2);

int comparePositionLayer(ecs_entity_t e1, const Position *p1, ecs_entity_t e2, const Position *p2);
