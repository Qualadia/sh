#pragma once

#include <raylib-cpp.hpp>
#include <string>
#include <vector>
#include "atlas_zone.hpp"

struct TextureAtlas {
  TextureAtlas(std::string fileName, int textureWidth, int textureHeight) : atlas{LoadTexture(fileName.c_str())} {
    for(int i = 0; i < atlas.height; i += textureHeight) {
      for(int j = 0; j < atlas.width; j += textureWidth) {
        textureRects.push_back({
          (float)j,
          (float)i,
          (float)textureWidth,
          (float)textureHeight
        });
      };
    };
  };

  ~TextureAtlas() {
    UnloadTexture(atlas);
  };

  Rectangle GetTextureRect(unsigned int id) {
    return textureRects[id];
  };
  private:
    const Texture2D atlas;
    std::vector<Rectangle> textureRects;
};
