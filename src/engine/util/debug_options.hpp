struct DebugOptions {
  bool textureWireframes = false;
  bool hitboxWireframes = false;
  bool textures = true;
  bool fps = false;
};
