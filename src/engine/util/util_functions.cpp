#include "util_functions.hpp"

int compareHitboxLayer(ecs_entity_t e1, const Hitbox *h1, ecs_entity_t e2, const Hitbox *h2) {
  //if (h1->layer != h2->layer) {
    return (h1->layer > h2->layer);
  //} else {
  //  return (h1->weight < h2->weight);
  //};
};

int comparePositionLayer(ecs_entity_t e1, const Position *p1, ecs_entity_t e2, const Position *p2) {
  if (p1->layer != p2->layer) {
    return (p1->layer > p2->layer);
  } else {
    return (p1->position.y + p1->relativeSpritePosition.y > p2->position.y + p2->relativeSpritePosition.y); 
  }
};
