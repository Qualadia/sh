#pragma once
#include "frame_pipeline.hpp"
#include "flecs.h"

class EngineController {
  public:
    EngineController();
    void StartGameLoop();
    void SetPlayer(flecs::entity* p);
    flecs::world* GetWorld();
  private:
    flecs::world mWorld;
    flecs::entity* mPlayer;
    FramePipeline mFramePipeline;
};
