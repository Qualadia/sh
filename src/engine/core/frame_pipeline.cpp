#include "frame_pipeline.hpp"
#include <iostream>
#include <raylib-cpp.hpp>
#include <raylib.h>
#include "imgui.h"
#include "rlImGui.h"

FramePipeline::FramePipeline(flecs::world* ecs) : mWorld{ecs},
                                                  mCollisionSystem(ecs, 1.0/60.0f),
                                                  mDebugSystem(ecs),
                                                  // mGui...,
                                                  mInputSystem(ecs),
                                                  mLogicSystem(ecs),
                                                  mRenderSystem(ecs){};


void FramePipeline::Update() {
  mWorld->progress();
};

