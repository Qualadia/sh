#include "engine_controller.hpp"
#include "frame_pipeline.hpp"
#include "raylib-cpp.hpp"
#include "imgui.h"
#include "rlImGui.h"
#include "../components/camera_component.hpp"

EngineController::EngineController() : mWorld{flecs::world()}, mFramePipeline{FramePipeline(&mWorld)} {
  InitWindow(1280, 720, "anim test");
  SetTargetFPS(60);
  rlImGuiSetup(true);
  ImGui::GetIO().ConfigWindowsMoveFromTitleBarOnly = true;
};

void EngineController::StartGameLoop() {
  flecs::ref<CameraDynamic> c = mPlayer->get_ref<CameraDynamic>();
  while (!WindowShouldClose())
    {
      BeginDrawing();
        
        BeginMode2D(c->camera);

          ClearBackground(GRAY);
          mFramePipeline.mInputSystem.mInputSystem.run();
          mFramePipeline.mLogicSystem.mLogicSystem.run();
          mFramePipeline.mCollisionSystem.mCollisionsystem.run();
          mFramePipeline.mRenderSystem.mRenderSystem.run();

          DrawFPS(0, 0);
        EndMode2D();
        rlImGuiBegin();
          mFramePipeline.mDebugSystem.mDebugSystem.run();
        rlImGuiEnd();
      
      EndDrawing();
    }
    CloseWindow();

};

void EngineController::SetPlayer(flecs::entity* p){
  mPlayer = p;
}

flecs::world* EngineController::GetWorld() {
  return &mWorld;
};
