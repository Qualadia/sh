#pragma once
#include <flecs.h>
#include "../systems/collision_system.hpp"
#include "../systems/debug_system.hpp"
#include "../systems/gui_system.hpp"
#include "../systems/input_system.hpp"
#include "../systems/logic_system.hpp"
#include "../systems/render_system.hpp"


class FramePipeline {
  public:
    FramePipeline(flecs::world* world);
    //~FramePipeline();
    void Update();
    CollisionSystem mCollisionSystem;
    DebugSystem mDebugSystem;
    // GuiSystem 
    InputSystem mInputSystem;
    LogicSystem mLogicSystem;
    RenderSystem mRenderSystem;
  private:
    flecs::world* mWorld;
};
