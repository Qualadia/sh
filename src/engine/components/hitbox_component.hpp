#pragma once

#include <raylib-cpp.hpp>
#include <box2d/box2d.h>

struct CollisionsWorld {
    b2World *world;
};

struct Hitbox {
  /* 
  std::vector<std::tuple<raylib::Rectangle, int, int>> hitboxes;
  */
  // this is a array of pair, in which(pair) first thing is rectangle for defining size of hitbox
  // and the first int is for layer, and the second int is for "weight" to define what will move what
  // if two entities have movable component, e.g. tentacle of a boss moves the player, not another way around 

  int layer;
  b2PolygonShape* hitbox;
  b2BodyType type;
};
