#pragma once

#include <flecs.h>
#include <functional>

struct Logic {
  Logic() { };
  Logic(std::function<void()>&& f) : executeLogic{std::move(f)} { };
  std::function<void()> executeLogic;
};
