#pragma once
#include <raylib-cpp.hpp>
#include <box2d/box2d.h>

struct Position {
  Position() {};
  Position(Vector2 p, Vector2 rsp, int l) : position{p}, relativeSpritePosition{rsp}, layer{l} {};
  Position(Vector2 p, Vector2 rsp, int l, b2Body* b) : position{p}, relativeSpritePosition{rsp}, layer{l}, body{b} {};
  
  Vector2 position;
  Vector2 relativeSpritePosition;
  int layer;
  b2Body* body;
};

