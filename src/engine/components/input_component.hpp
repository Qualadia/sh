#pragma once
#include <raylib-cpp.hpp>
#include <map>


struct Input {
  Input() { };
  Input(
      std::vector<KeyboardKey> kjpk,
      std::vector<KeyboardKey> kpk,
      std::vector<KeyboardKey> kjrk,
      std::vector<GamepadButton> gjpk,
      std::vector<GamepadButton> gpk,
      std::vector<GamepadButton> gjrk,
      std::vector<MouseButton> mjpk,
      std::vector<MouseButton> mpk,
      std::vector<MouseButton> mjrk
      ) {

    for (auto i : kjpk) { keyboardJustPressedKeys[i] = false; };
    for (auto i : kpk) { keyboardPressedKeys[i] = false; };
    for (auto i : kjrk) {keyboardJustReleasedKeys[i] = false; };
    
    for (auto i : gjpk) { gamepadJustPressedKeys[i] = false; };
    for (auto i : gpk) { gamepadPressedKeys[i] = false; };
    for (auto i : gjrk) { gamepadJustReleasedKeys[i] = false; };

    for (auto i : mjpk) { mouseJustPressedKeys[i] = false; };
    for (auto i : mpk) { mousePressedKeys[i] = false; };
    for (auto i : mjrk) { mouseJustReleasedKeys[i] = false; }

  };
 
  std::map<KeyboardKey, bool> keyboardJustPressedKeys;
  std::map<KeyboardKey, bool> keyboardPressedKeys;
  std::map<KeyboardKey, bool> keyboardJustReleasedKeys;
  std::map<GamepadButton, bool> gamepadJustPressedKeys;
  std::map<GamepadButton, bool> gamepadPressedKeys;
  std::map<GamepadButton, bool> gamepadJustReleasedKeys;
  std::map<MouseButton, bool> mouseJustPressedKeys;
  std::map<MouseButton, bool> mousePressedKeys;
  std::map<MouseButton, bool> mouseJustReleasedKeys;

  Vector2 leftStick;
  Vector2 rightStick;
  Vector2 mousePosition;
  bool gamepadPreffered;
};
