#pragma once
#include "raylib-cpp.hpp"

struct CameraDynamic {
  CameraDynamic() { };
  CameraDynamic(raylib::Camera2D c) : camera{std::move(c)} { };
  CameraDynamic(raylib::Camera2D c, 
                  raylib::Vector2 offset,
                  raylib::Vector2 target,
                  float rotation,
                  float zoom) : camera{std::move(c)} {
    camera.offset = offset;
    camera.target = target;
    camera.rotation = rotation;
    camera.zoom = zoom;
  };
  raylib::Camera2D camera;
};
