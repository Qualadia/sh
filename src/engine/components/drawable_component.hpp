#pragma once

#include <flecs.h>
#include <raylib-cpp.hpp>
#include "../util/advanced_texture_atlas.hpp"

struct DrawableSprite {
  AdvancedTextureAtlas* atlas;
  unsigned int zone;
  unsigned int textureID;
};


struct DrawableAnimation {
  AdvancedTextureAtlas* atlas;
  unsigned int zone;
  unsigned int startFrame;
  unsigned int endFrame;
  float frameSpeed;
  Vector2 relativePosition; 
  unsigned int frameCounter;
  unsigned int currentFrame;
};
/*
struct DrawableArrayAnimation {
  AdvancedTextureAtlas* Atlas;
  std::vector<std::pair<int, int>> zonesAndEndframes;
  float FrameSpeed = 10;
  float scale = 1.0f;
  int CurrentAnimation = 0;
  virtual void Draw(CameraComponent* camera, ECSManager* manager);
  virtual void PlayAnimation(int animation);
  private:
    int FramesCounter = 0;
    int CurrentFrame = 0;
};
*/


