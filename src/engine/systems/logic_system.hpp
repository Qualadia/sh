#pragma once
#include <flecs.h>
#include "../components/logic_component.hpp"

struct LogicSystem {
  flecs::system mLogicSystem;
  LogicSystem(flecs::world* w);
};
