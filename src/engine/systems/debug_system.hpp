#pragma once
#include <flecs.h>

struct DebugSystem {
  flecs::system mDebugSystem;
  DebugSystem(flecs::world* w);
};
