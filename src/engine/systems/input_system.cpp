#include "input_system.hpp"

InputSystem::InputSystem(flecs::world* w) {
  mInputSystem = w->system<Input>()
    .each([] (flecs::entity e, Input& in) {
      if (IsGamepadAvailable(1)) {
        for (auto& i : in.gamepadJustPressedKeys) {
          i.second = IsGamepadButtonPressed(1 ,i.first);
        };
        for (auto& i : in.gamepadPressedKeys) {
          i.second = IsGamepadButtonDown(1, i.first);
        };
        for (auto& i : in.gamepadJustReleasedKeys) {
          i.second = IsGamepadButtonUp(1, i.first);
        };
        in.leftStick = {
          GetGamepadAxisMovement(1,0),
          GetGamepadAxisMovement(1,1)
        };
        in.rightStick = {
          GetGamepadAxisMovement(1,2),
          GetGamepadAxisMovement(1,3)
        };
        in.gamepadPreffered = true;
      } else {
        in.gamepadPreffered = false;
      };

      for (auto& i : in.keyboardJustPressedKeys) {
        i.second = IsKeyPressed(i.first);
      };
      for (auto& i : in.keyboardPressedKeys) {
        i.second = IsKeyDown(i.first);
      };
      for (auto& i : in.keyboardJustReleasedKeys) {
        i.second = IsKeyReleased(i.first);
      };
      for (auto& i : in.mouseJustPressedKeys) {
        i.second = IsMouseButtonReleased(i.first);
      };
      for (auto& i : in.mousePressedKeys) {
        i.second = IsMouseButtonDown(i.first);
      };
      for (auto& i : in.mouseJustReleasedKeys) {
        i.second = IsMouseButtonReleased(i.first);
      };

      in.mousePosition = GetMousePosition();
    });
};
