#include "render_system.hpp"
#include "../components/drawable_component.hpp"
#include "../components/position_component.hpp"
#include "../components/hitbox_component.hpp"
#include "../util/util_functions.hpp"

RenderSystem::RenderSystem(flecs::world* w) {
  mRenderSystem = w->system<Position>()
    .term<DrawableSprite>().oper(flecs::Or)
    .term<DrawableAnimation>().oper(flecs::Or)
    .order_by(comparePositionLayer)
    .each([](flecs::entity e, Position& p) {
      if (e.has<DrawableSprite>()) {
        const DrawableSprite *sprite = e.get<DrawableSprite>();
        DrawTextureRec( sprite->atlas->atlas, 
                        sprite->atlas->GetTextureRect(sprite->zone, sprite->textureID), 
                        Vector2Add(p.relativeSpritePosition, p.position), 
                        WHITE);
      } else if (e.has<DrawableAnimation>()) {
        const DrawableAnimation *animation = e.get<DrawableAnimation>();
        unsigned int currentFrameTemp = animation->currentFrame; 
        unsigned int frameCounterTemp = animation->frameCounter+1;
        if (frameCounterTemp >= 60/animation->frameSpeed) {
          frameCounterTemp = 0;
          currentFrameTemp++;
          if (currentFrameTemp >= animation->endFrame) currentFrameTemp = 0;
        };
        e.set<DrawableAnimation>({
          animation->atlas,
          animation->zone,
          animation->startFrame,
          animation->endFrame,
          animation->frameSpeed,
          animation->relativePosition,
          frameCounterTemp,
          currentFrameTemp
        });
        DrawTextureRec(
          animation->atlas->atlas,
          animation->atlas->GetTextureRect(animation->zone, currentFrameTemp),
          Vector2Add(p.relativeSpritePosition, p.position),
          WHITE
        );
    };
  });
};
