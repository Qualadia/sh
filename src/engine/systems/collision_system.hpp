#pragma once
#include <flecs.h>
#include <box2d/box2d.h>
#include "../components/position_component.hpp"
#include "../components/hitbox_component.hpp"
#include "../util/util_functions.hpp"


// Vector2 ComputeCollisions(flecs::iter it, Hitbox *h, Movable *m, Position *p);

struct CollisionSystem {
  CollisionSystem(flecs::world* objectWorld, float timeStep);
  int32 velocityIterations = 15;
  int32 positionIterations = 15;
  b2World* collisionWorld;
  float collisionTimeStep;
  unsigned int collisionWorldScaleFactor = 5;
  flecs::system mCollisionsystem;
};
