#include "collision_system.hpp"
#include <iostream>


CollisionSystem::CollisionSystem(flecs::world* ow, float ts) {
  b2World *pworld = new b2World({0, 0});
  ow->set<CollisionsWorld>({pworld});

  ow->observer<Hitbox>("OnSetHitbox").event(flecs::OnSet)
        .each([ow, this](flecs::entity e, Hitbox& h) {
            const Position *p = e.get<Position>();
            b2World *world = ow->get<CollisionsWorld>()->world;
            
            b2BodyDef bodyDef;
            bodyDef.type = h.type;
            bodyDef.position =  b2Vec2(
              p->position.x/collisionWorldScaleFactor, 
              p->position.y/collisionWorldScaleFactor);
            // bodyDef.allowSleep = false;
            bodyDef.fixedRotation = true;
            bodyDef.linearDamping = 40.0f;
            
            b2Body* dbody = world->CreateBody(&bodyDef);
            b2Vec2 scaledShape[h.hitbox->m_count];
            for (int i = 0; i<= h.hitbox->m_count; i++) {
            scaledShape[i] = {
              h.hitbox->m_vertices[i].x/collisionWorldScaleFactor,
              h.hitbox->m_vertices[i].y/collisionWorldScaleFactor};
            };
            
            b2PolygonShape scaledHitbox;
            scaledHitbox.Set(scaledShape, h.hitbox->m_count);
            
            b2FixtureDef fixtureDef;
            fixtureDef.shape = &scaledHitbox;
            fixtureDef.density = 0.5f;
            fixtureDef.friction = 0;
            fixtureDef.restitution = 0.2;
            // fixtureDef.restitutionThreshold = 50000;
            
            dbody->CreateFixture(&fixtureDef);

            world->CreateBody(&bodyDef);
            e.set<Position>({p->position, p->relativeSpritePosition, p->layer, dbody});
        }    
    );
  
  
  flecs::query<Position> q = ow->query_builder<Position>().build();
  mCollisionsystem = ow->system<CollisionsWorld>()
    .each([q, this](flecs::entity it, CollisionsWorld& w){
      w.world->Step(1.0f/60.0f, this->velocityIterations, this->positionIterations);
      // w.world->Step(1.0f/60.0f, 5000, 5000);
      q.each([this](flecs::entity e, Position& p) {
        b2Vec2 pos = p.body->GetPosition();
        p.position = {pos.x*collisionWorldScaleFactor, pos.y*collisionWorldScaleFactor};
      });
    });
}