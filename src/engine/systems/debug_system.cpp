#include "debug_system.hpp"
#include <raylib-cpp.hpp>
#include "../components/hitbox_component.hpp"
#include "../components/position_component.hpp"
#include "../components/lua_component.hpp"
#include "imgui.h"
#include "misc/cpp/imgui_stdlib.h"
#include <iostream>
#include <string>

class luaImGuiConsole {
  public:
    luaImGuiConsole() {
      // luaState.open_libraries(sol::lib::base, sol::lib::package);
    };
    void update() {
      const float footer_height_to_reserve = ImGui::GetStyle().ItemSpacing.y + ImGui::GetFrameHeightWithSpacing();
      if (ImGui::BeginChild("ScrollingRegion", ImVec2(0, -footer_height_to_reserve), false, ImGuiWindowFlags_HorizontalScrollbar)) {
      // if (ImGui::BeginPopupContextWindow())
      // {
      //     if (ImGui::Selectable("Clear")) ClearLog();
      //     ImGui::EndPopup();
      // }
      // ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2(4, 1)); // Tighten spacing
      // if (copy_to_clipboard)
      //     ImGui::LogToClipboard();
        for (std::pair<std::string,std::pair<std::string,bool>> item: this->History) {
          // const char* item = Items[i];
          // if (!Filter.PassFilter(item))
          //     continue;

          // Normally you would store more information in your item than just a string.
          // (e.g. make Items[] an array of structure, store color/type etc.)
          ImVec4 color;
          bool has_color = false;
          if(!item.second.second) { color = ImVec4(1.0f, 0.4f, 0.4f, 1.0f); has_color = true;}
          // if (strstr(item, "[error]")) { color = ImVec4(1.0f, 0.4f, 0.4f, 1.0f); has_color = true; }
          // else if (strncmp(item, "# ", 2) == 0) { color = ImVec4(1.0f, 0.8f, 0.6f, 1.0f); has_color = true; }
          if (has_color) ImGui::PushStyleColor(ImGuiCol_Text, color);
          
          std::string answer = ">> " + item.second.first;
          ImGui::TextUnformatted(item.first.c_str());
          ImGui::TextUnformatted(answer.c_str());
        if (has_color) ImGui::PopStyleColor();
        }
        // if (copy_to_clipboard)
        //     ImGui::LogFinish();
        // Keep up at the bottom of the scroll region if we were already at the bottom at the beginning of the frame.
        // Using a scrollbar or mouse-wheel will take away from the bottom edge.
        if (ScrollToBottom || (AutoScroll && ImGui::GetScrollY() >= ImGui::GetScrollMaxY()))
          ImGui::SetScrollHereY(1.0f);
        ScrollToBottom = false;
        // ImGui::PopStyleVar();
      }
      ImGui::EndChild();
      ImGui::Separator();
      ImGuiInputTextFlags input_text_flags = ImGuiInputTextFlags_EnterReturnsTrue | ImGuiInputTextFlags_EscapeClearsAll | ImGuiInputTextFlags_CallbackCompletion | ImGuiInputTextFlags_CallbackHistory;
      bool reclaim_focus = false;
      if (ImGui::InputText("Input", &InputBuf, input_text_flags, [](ImGuiInputTextCallbackData* data) -> int {return 1;}, (void*)this)) {
        // auto result = luaState.safe_script(InputBuf, sol::script_pass_on_error);
        // History.push_back(std::pair<std::string,std::pair<std::string,bool>>{InputBuf, {result, result.valid()}});
        InputBuf = "";
        reclaim_focus = true;
      }
      ImGui::SetItemDefaultFocus();
      if (reclaim_focus)
          ImGui::SetKeyboardFocusHere(-1); // Auto focus previous widget

    };
    int TextEditCallbackStub(ImGuiInputTextCallbackData* data) {
      std::cout << data->Buf << std::endl;
      return 1;
    };

  private:
    // std::vector<std::string> InputHistory;
    std::vector<std::pair<std::string,std::pair<std::string,bool>>> History;
    std::string InputBuf;
    // sol::state luaState;
    bool AutoScroll = true;      
    bool ScrollToBottom = false;
};
// luaImGuiConsole::luaImGuiConsole(){};

DebugSystem::DebugSystem(flecs::world* w) {
    bool opened = false;
    bool opened1 = false;
    std::string           InputBuf;
    ImVector<char*>       Items;
    ImVector<const char*> Commands;
    ImVector<char*>       History;
    int                   HistoryPos;    // -1: new line, 0..History.Size-1 browsing history.
    ImGuiTextFilter       Filter;
    bool                  AutoScroll;
    bool                  ScrollToBottom;

    luaImGuiConsole *console = new luaImGuiConsole();
    // Font placeholderFont = LoadFont("placeholderFont");
    mDebugSystem = w->system<CollisionsWorld>()
      .iter([&opened, &opened1, InputBuf, console](flecs::iter &it, CollisionsWorld *clw) {
        ImGui::ShowDemoWindow(&opened1);
        
        // Create a window called "My First Tool", with a menu bar.
        ImGui::Begin("My First Tool", &opened);
        console->update();
        ImGui::End();
        
        // std::vector<std::pair<int, int>> objects;
        //for (auto i : it) {
        //  objects.push_back({it.entity(i).id(), 0});
        //  for (auto hitboxin : h[i].hitbox) {
        //    DrawRectangleLinesEx(
        //    {p[i].position.x + hitboxin.x,
        //    p[i].position.y + hitboxin.y,
        //    hitboxin.width,
        //    hitboxin.height},
        //    2.0f,
        //    PURPLE);
        //  };
        //  DrawTextEx(
        //    placeholderFont,
        //    ("X: " + std::to_string(static_cast<int>(p[i].position.x)) + 
        //    "\nY:" + std::to_string(static_cast<int>(p[i].position.y)) + 
        //    "\nZ:" + std::to_string(static_cast<int>(p[i].layer))).c_str(), 
        //    {p[i].position.x, 
        //    p[i].position.y - 64}, 
        //    16.0, 3.0, PURPLE);
        //};
        b2World *world = clw->world;
        b2Body *iter = world->GetBodyList();
        while (iter != nullptr) {
          b2Fixture *fixes = iter->GetFixtureList();
          while (fixes != nullptr) {
            b2Shape *poly = fixes->GetShape();
            switch(poly->GetType()) {
              case b2Shape::e_chain:
                break;
              case b2Shape::e_circle:
                break;
              case b2Shape::e_edge:
                break;
              case b2Shape::e_polygon: {
                b2PolygonShape *shape = dynamic_cast<b2PolygonShape*>(poly);
                for (int i = 0; i <= shape->m_count-1; i++) {
                  Vector2 lineStart = {iter->GetPosition().x + shape->m_vertices[i].x,
                                      iter->GetPosition().y + shape->m_vertices[i].y};
                  Vector2 lineEnd;
                  if (i == shape->m_count-1) {
                    lineEnd = {iter->GetPosition().x + shape->m_vertices[0].x,
                              iter->GetPosition().y + shape->m_vertices[0].y};
                  } else {
                    lineEnd = {iter->GetPosition().x + shape->m_vertices[i+1].x,
                              iter->GetPosition().y + shape->m_vertices[i+1].y};
                  };                                
                  DrawLineV(lineStart, lineEnd, RED);
                }
                break;
              }
              case b2Shape::e_typeCount:
                break;
              default:
                break;
              };
              fixes = fixes->GetNext();
            };
            iter = iter->GetNext();
        }
    });

}
