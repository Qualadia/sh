#include "logic_system.hpp"

LogicSystem::LogicSystem(flecs::world* w) {
  mLogicSystem = w->system<Logic>()
    .iter([](flecs::iter it, Logic *l) {
      for(auto i : it) {
        l[i].executeLogic();
      }
    });
};
