#pragma once
#include <flecs.h>
#include "../components/input_component.hpp"

struct InputSystem {
  InputSystem(flecs::world* w);
  flecs::system mInputSystem;
};
